#include<memory>
#include<utility>
#include <initializer_list>
#include<vector>
#include<functional>
#include<random>
#include<iostream>
#include<fstream>
#include<algorithm>
#include"DiagonalMatrix.h"
#include"vectorOperations.h"
#include"tridiagonal.h"

using namespace std;

DiagonalMatrix generateMatrix(int N, double a, double b)
{
    double hx = (b-a)/(N-1);
    std::vector<double> x(N);
    for(int i = 0 ; i < N ; i++)
    {
        x[i] = a + hx*i;
    }
    std::vector<double> diag0(N,-0.5/(hx*hx));
    diag0[0] = -1;
    std::vector<double> diag1(1.0/(hx*hx) + 0.5*x*x);
    std::vector<double> diag2(N,-0.5/(hx*hx));
    *(diag2.end() - 1) = -1;
    DiagonalMatrix res({diag0, diag1, diag2}, {-1,0,1}, N);
    return res;
}

std::vector<double> generateRHS(const std::vector<double> &x, std::function<double(double)> f)
{
    std::vector<double> res;
    for(auto xVal: x)
    {
        res.push_back(f(xVal));
    }
    return res;
}

void csvFormat(ofstream &file, const vector<double>& x, const vector<double>& y)
{
    for(int i = 0 ; i < x.size() ; i++)
    {
        file << x[i] << " " << y[i] << endl;
    }
    file << endl;
}

int main()
{
    std::random_device r;
    int N = 200;
    double a = -5;
    double b = 5;
    double hx = (b-a)/(N-1);
    std::vector<double> x(N);
    for(int i = 0 ; i < N ; i++)
    {
        x[i] = a + hx*i;
    }
    DiagonalMatrix A(generateMatrix(N, a, b));
    cout << A << endl;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(10.0, 10000.0);
    std::vector<double> v1(N, 100);
    std::vector<double> v2(N, 100);
    std::generate(v1.begin(), v1.end(), [&](){return uniform_dist(e1);});
    //std::cout << v1 << endl;
    double lambda;
    for(int i = 0 ; i < 100 ; i++)
    {
        std::vector<double> alpha = generateAlpha(A);
        std::vector<double> beta = generateBeta(A, v1, alpha);
        v2 = solve(alpha, beta);
        //cout << "check " << norm(A*v2 - v1) << endl;
        lambda = norm(v1)/norm(v2);
        v2=v2*0.5;
        //std::cout << norm(v2) << " " << lambda << std::endl;
        v1.swap(v2);
    }
    //std::cout << v2 << std::endl;
    std::cout << norm(v2) << " " << lambda << std::endl;
    std::cout << norm(A*v2-lambda*v2) << endl << endl;
    //std::cout << A*v2 << endl << lambda*v2 << endl;
    ofstream file("data.csv");
    csvFormat(file, x, v2/sqrt(integral(v2,hx)));
    file.close();
    std::cout << integral(v2/sqrt(integral(v2,hx)),hx) << endl;
}
