#pragma once
#include<vector>
#include<iostream>

struct DiagonalMatrix
{
    std::vector<std::vector<double> > diagonals;
    std::vector<double> offsets;
    int N;
    DiagonalMatrix()
    : diagonals(), offsets(), N(0)
    {
    }
    DiagonalMatrix(const std::vector<std::vector<double> > &diagonals, const std::vector<double> &offsets, const int &N)
    : diagonals(diagonals), offsets(offsets), N(N)
    {
    }
    DiagonalMatrix(DiagonalMatrix&& M)
    : diagonals(std::move(M.diagonals)), offsets(std::move(M.offsets)), N(std::move(M.N))
    {
    }
    DiagonalMatrix& operator=(const DiagonalMatrix& m)
    {
        diagonals = m.diagonals;
        offsets = m.offsets;
        N = m.N;
        return *this;
    }
    DiagonalMatrix& operator=(DiagonalMatrix&& m)
    {
        diagonals = std::move(m.diagonals);
        offsets = std::move(m.offsets);
        N = std::move(m.N);
        return *this;
    }
};

std::vector<double> operator*(const DiagonalMatrix& M, const std::vector<double>& v)
{
    std::vector<double> res(M.N);
    for(int i = 0 ; i < M.N ; i++)
    {
        for(int j = 0 ; j < M.offsets.size() ; j++)
        {
            int index = M.offsets[j]+i;
            if(index >= 0 and index < M.N)
            {
                res[i]+=M.diagonals[j][i]*v[index];
            }
        }
    }
    return res;
}



std::ostream& operator<<(std::ostream& out, const DiagonalMatrix &M)
{
    for(int i = 0 ; i < M.N ; i++)
    {
        for(int j = 0 ; j < M.offsets.size() ; j++)
        {
            int index = M.offsets[j]+i;
            if(index >= 0 and index < M.N)
            {
                out << M.diagonals[j][i] << " ";
            }
            else
            {
                out << " - ";
            }
        }
        out << std::endl;
    }
    return out;
}
