#pragma once

#include"DiagonalMatrix.h"
#include<vector>

std::vector<double> generateAlpha(const DiagonalMatrix& M)
{
    std::vector<double> alpha;
    const std::vector<double> &a = M.diagonals[0];
    const std::vector<double> &b = M.diagonals[1];
    const std::vector<double> &c = M.diagonals[2];
    alpha.push_back(c[0]/b[0]);
    for(int i = 1 ; i < M.N-1 ; i++)
    {
        alpha.push_back(c[i]/(b[i] - a[i]*alpha[i-1]));
    }
    return alpha;
}

std::vector<double> generateBeta(const DiagonalMatrix& M, const std::vector<double>& rhs, const std::vector<double>& alpha)
{
    std::vector<double> beta;
    const std::vector<double> &a = M.diagonals[0];
    const std::vector<double> &b = M.diagonals[1];
    const std::vector<double> &c = M.diagonals[2];
    beta.push_back(rhs[0]/b[0]);
    for(int i = 1 ; i < M.N ; i++)
    {
        beta.push_back((rhs[i] - a[i]*beta[i-1])/(b[i] - a[i]*alpha[i-1]));
    }
    return beta;
}

std::vector<double> solve(const std::vector<double>& alpha, const std::vector<double>& beta)
{
    std::vector<double> res(beta.size());
    *(res.end() - 1) = *(beta.end() - 1);
    for(int i = beta.size()-2 ; i >= 0 ; i--)
    {
        res[i] = beta[i] - alpha[i]*res[i+1];
    }
    return res;
}
