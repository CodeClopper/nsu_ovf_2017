#pragma once
#include<vector>

struct Matrix
{
    std::vector<double> values;
    int N, M; //rows, columns
    Matrix(int N, int M, std::initializer_list<double> values)
    : N(N), M(M), values(values)
    {}
    Matrix(const Matrix& A)
    : N(A.N), M(A.M), values(A.values)
    {}
    Matrix(Matrix&& A)
    : N(std::move(A.N)), M(std::move(A.M)), values(std::move(A.values))
    {}
    Matrix& operator=(const Matrix& A)
    {
        N = A.N;
        M = A.M;
        values = A.values;
        return *this;
    }
    Matrix& operator=(Matrix&& A)
    {
        N = std::move(A.N);
        M = std::move(A.M);
        values = std::move(A.values);
        return *this;
    }
    double& get(int i, int j)
    {
        return values[i*M+j];
    }
    const double& get(int i, int j) const
    {
        return values[i*M+j];
    }
}

vector<double> operator*(const Matrix& A, const std::vector<double>& b)
{
    std::vector<double> res(A.N);
    for(int i = 0 ; i < A.N ; i++)
    {
        double val = 0;
        for(int j = 0 ; j < b.size() ; j++)
        {
            val += A.get(i,j)*b[j];
        }
        res[i] = val;
    }
    return std::move(res);
}

vector<double> operator*(const std::vector<double>& b, const Matrix& A)
{
    std::vector<double> res(A.M);
    for(int j = 0 ; j < A.M ; j++)
    {
        double val = 0;
        for(int i = 0 ; i < b.size() ; i++)
        {
            val += A.get(i,j)*b[i];
        }
        res[j] = val;
    }
    return std::move(res);
}
