#include<iostream>
#include<functional>
#include<vector>
#include<fstream>
#include<utility>
#include<cmath>
#include<algorithm>

using namespace std;

double dxdt(double x)
{
    return -x;
}

double exactSolution(double x)
{
    return exp(-x);
}

double euler(double dt, double x, std::function<double(double)> dxdt)
{
    return x + dxdt(x)*dt;
}

double rungeKutt(double dt, double x, std::function<double(double)> dxdt)
{
    return x + dt*dxdt(x + 0.5*dt*dxdt(x));
}

vector<std::pair<double,double> > solve(double x,
                                        std::pair<double,double> tSeg,
                                        double dt,
                                        std::function<double(double)> dxdt,
                                        std::function<double(double,double,std::function<double(double)>)> method)
{
    vector<std::pair<double,double> >solution;
    double t = tSeg.first;
    while(true)
    {
        solution.push_back(make_pair(t,x));

        t += dt;
        if(t > tSeg.second)
        {
            x = method(tSeg.second - (t - dt), x, dxdt);
            t = tSeg.second;
            break;
        }
        x = method(dt, x, dxdt);
    }
    solution.push_back(make_pair(t,x));
    return solution;
}

void csvHeader(ofstream &file, std::pair<string,string> names)
{
    file << names.first << ", " << names.second << endl;
}

void csvFormat(ofstream &file, std::pair<double,double> values)
{
    file << values.first << ", " << values.second << endl;
}

void saveData(string name, vector<std::pair<double,double> > values, std::function<void(ofstream &,std::pair<double,double>)> format)
{
    ofstream file(name);

    for (auto value: values)
    {
        csvFormat(file, value);
    }

    file.close();
}



int main()
{
    double x = 1;
    double t = 0;
    double tLast = 3;
    double dt = 0.1;


    vector<std::pair<double,double> >solutionEuler;
    vector<std::pair<double,double> >solutionRunge;
    //vector<std::pair<double,double> >solutionExact;

    solutionEuler = solve(x,make_pair(t,tLast),dt,dxdt,euler);
    solutionRunge = solve(x,make_pair(t,tLast),dt,dxdt,rungeKutt);

    saveData("dataEuler.csv", solutionEuler, csvFormat);
    saveData("dataRunge.csv", solutionRunge, csvFormat);


    int NExact = solutionEuler.size();
    vector<std::pair<double,double> >solutionExact;
    vector<std::pair<double,double> >solutionErrorEuler;
    vector<std::pair<double,double> >solutionErrorRunge;

    for(int i = 0 ; i < solutionEuler.size() ; i++)
    {
        solutionExact.push_back(make_pair(solutionEuler[i].first, exactSolution(solutionEuler[i].first)));
        solutionErrorEuler.push_back(make_pair(solutionEuler[i].first, fabs(solutionExact[i].second-solutionEuler[i].second)));
        solutionErrorRunge.push_back(make_pair(solutionEuler[i].first, fabs(solutionExact[i].second-solutionRunge[i].second)));
    }

    /*std::function<std::pair<double,double>()> exactSolutionGenerator = [N=NExact] {
        const double a = 0;
        const double b = 3;
        const double dt = (b-a)/(N-1);
        double t = 0;
        double x = 1;
        return [=]() mutable{
            auto pairExact = make_pair(t,x);
            t = t+dt;
            x = exactSolution(t);
            return pairExact;
        };
    }();
    std::generate(solutionExact.begin(), solutionExact.end(), exactSolutionGenerator);*/
    saveData("dataExact.csv", solutionExact, csvFormat);
    saveData("errorEuler.csv", solutionErrorEuler, csvFormat);
    saveData("errorRunge.csv", solutionErrorRunge, csvFormat);
    //vector<std::pair<double,double> >solutionErrorEuler(solutionRunge.size());
    //transform(solutionEuler.begin(),solutionEuler.end(),solutionExact);
}
