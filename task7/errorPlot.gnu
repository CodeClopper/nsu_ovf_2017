
set title "dx/dt = -x ; t=[0:3] ; N = 31 ; absolute error"

set ylabel "absolute error"
set xlabel "t"

plot "errorEuler.csv" with lines title "Euler 1st order", "errorRunge.csv" with lines title "Runge Kutta 2nd order"
