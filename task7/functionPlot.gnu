
set title "dx/dt = -x ; t=[0:3] ; N = 31 ; function x"

set ylabel "x(t)"
set xlabel "t"

plot "dataEuler.csv" with lines title "Euler 1st order", "dataRunge.csv" with lines title "Runge Kutta 2nd order", "dataExact.csv" with lines title "Exact solution"
