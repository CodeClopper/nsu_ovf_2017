#include<cmath>
#include<functional>
#include<vector>
#include<utility>
#include<algorithm>
#include<ostream>
#include<iostream>
#include<fstream>

using namespace std;

struct DiagonalMatrix
{
    vector<vector<double> > diagonals;
    vector<double> offsets;
    int N;
    DiagonalMatrix()
    : diagonals(), offsets(), N(0)
    {
    }
    DiagonalMatrix(const vector<vector<double> > &diagonals, const vector<double> &offsets, const int &N)
    : diagonals(diagonals), offsets(offsets), N(N)
    {
    }
    DiagonalMatrix(DiagonalMatrix&& M)
    : diagonals(std::move(M.diagonals)), offsets(std::move(M.offsets)), N(std::move(M.N))
    {
    }
    DiagonalMatrix& operator=(const DiagonalMatrix& m)
    {
        diagonals = m.diagonals;
        offsets = m.offsets;
        N = m.N;
        return *this;
    }
    DiagonalMatrix& operator=(DiagonalMatrix&& m)
    {
        diagonals = std::move(m.diagonals);
        offsets = std::move(m.offsets);
        N = std::move(m.N);
        return *this;
    }
};

vector<double> operator*(const DiagonalMatrix& M, const vector<double>& v)
{
    vector<double> res(M.N);
    for(int i = 0 ; i < M.N ; i++)
    {
        for(int j = 0 ; j < M.offsets.size() ; j++)
        {
            int index = M.offsets[j]+i;
            if(index >= 0 and index < M.N)
            {
                res[i]+=M.diagonals[j][i]*v[index];
            }
        }
    }
    return res;
}

std::ostream& operator<<(std::ostream& out, const vector<double> &vec)
{
    for(auto v: vec)
    {
        out << v << endl;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const DiagonalMatrix &M)
{
    for(int i = 0 ; i < M.N ; i++)
    {
        for(int j = 0 ; j < M.offsets.size() ; j++)
        {
            int index = M.offsets[j]+i;
            if(index >= 0 and index < M.N)
            {
                out << M.diagonals[j][i] << " ";
            }
            else
            {
                out << " - ";
            }
        }
        out << endl;
    }
    return out;
}

vector<double> generateAlpha(const DiagonalMatrix& M)
{
    vector<double> alpha;
    const vector<double> &a = M.diagonals[0];
    const vector<double> &b = M.diagonals[1];
    const vector<double> &c = M.diagonals[2];
    alpha.push_back(c[0]/b[0]);
    for(int i = 1 ; i < M.N-1 ; i++)
    {
        alpha.push_back(c[i]/(b[i] - a[i]*alpha[i-1]));
    }
    return alpha;
}

vector<double> generateBeta(const DiagonalMatrix& M, const vector<double>& rhs, const vector<double>& alpha)
{
    vector<double> beta;
    const vector<double> &a = M.diagonals[0];
    const vector<double> &b = M.diagonals[1];
    const vector<double> &c = M.diagonals[2];
    beta.push_back(rhs[0]/b[0]);
    for(int i = 1 ; i < M.N ; i++)
    {
        beta.push_back((rhs[i] - a[i]*beta[i-1])/(b[i] - a[i]*alpha[i-1]));
    }
    return beta;
}

vector<double> solve(const vector<double>& alpha, const vector<double>& beta)
{
    vector<double> res(beta.size());
    *(res.end() - 1) = *(beta.end() - 1);
    for(int i = beta.size()-2 ; i >= 0 ; i--)
    {
        res[i] = beta[i] - alpha[i]*res[i+1];
    }
    return res;
}

DiagonalMatrix generateMatrix(double hx, int N)
{
    vector<double> diag0(N, 1.0/(hx*hx));
    diag0[0] = -1;
    vector<double> diag1(N,-2.0/(hx*hx));
    vector<double> diag2(N, 1.0/(hx*hx));
    *(diag2.end() - 1) = -1;
    DiagonalMatrix res({diag0, diag1, diag2}, {-1,0,1}, N);
    return res;
}

vector<double> generateRHS(const vector<double> &x, std::function<double(double)> f)
{
    vector<double> res;
    for(auto xVal: x)
    {
        res.push_back(f(xVal));
    }
    return res;
}

double f(const double& x)
{
    return sin(x);
}

void csvFormat(ofstream &file, vector<double> x, vector<double> y)
{
    for(int i = 0 ; i < x.size() ; i++)
    {
        file << x[i] << " " << y[i] << endl;
    }
    file << endl;
}

int main()
{
    const vector<double> seg = {0, M_PI};
    int N = 10;
    double dx = (seg[1]-seg[0])/(N+1);
    double leftX = 0;
    double rightX = -1;
    vector<double> x(N);
    for(int i = 0 ; i < N ; i++)
    {
        x[i] = seg[0]+dx*(i+1);
    }
    DiagonalMatrix M = generateMatrix(dx, N);
    cout << M << endl;
    vector<double> rhs = generateRHS(x, f);
    rhs[0]+=-leftX/(dx*dx);
    *(rhs.end()-1)+=-rightX/(dx*dx);
    cout<< rhs << endl;
    vector<double> alpha = generateAlpha(M);
    vector<double> beta = generateBeta(M, rhs, alpha);
    vector<double> res = solve(alpha, beta);
    cout << res << endl;
    cout << M*res << endl;
    x.push_back(seg[1]);
    x.insert(x.begin(),seg[0]);
    res.push_back(rightX);
    res.insert(res.begin(),leftX);
    ofstream file("data.csv");
    csvFormat(file, x, res);
    file.close();
}
