#include<cstdio>
#include<bitset>
#include<iostream>

using namespace std;

union blabla
{
    float val1;
    unsigned int val2;
};

int main()
{
    float val = 1.0;
    printf("val = %f\n",val);
    float eps = 1;
    blabla binVal = {eps};
    cout << "eps = " << bitset<32>(binVal.val2) << endl;
    /*for(int i = 0 ; i < 30 ; i++)
    {
        printf("i = %d, eps = %10.8f, val+eps = %10.8f, val+eps/2 = %10.8f, val+eps+eps/2 = %10.8f, val+eps/2+eps = %10.8f\n", i,eps,val+eps,val+eps/2.0f,val+eps+eps/2.0f,val+eps/2.0f+eps);
        blabla binVal = {eps};
        cout << "eps = " << bitset<32>(binVal.val2) << endl;
        if(val!=val+eps and val==val+eps/2.0f)
        {
            printf("eps = %10.8f\n",eps);
        }
        eps /= 2.0;
    }*/
}
