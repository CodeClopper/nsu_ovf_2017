#include<cmath>
#include<vector>
#include<utility>
#include<functional>
#include<numeric>
#include<iostream>
#include<fstream>
#include<algorithm>

using namespace std;

double xDep(double k, double n)
{
    return M_PI*k/(4.0*n);
}

double initFunc(double x)
{
    return sin(x);
}

class PolynomialInterpolation
{
    public:
        vector<double> x;
        vector<double> y;
        vector<double> coefs;
        PolynomialInterpolation(vector<double> x, vector<double> y)
        : x(x), y(y)
        {
            coefs.resize(x.size());
            calcCoefs();
        }
        void calcCoefs()
        {
            for(int i = 0 ; i < coefs.size() ; i++)
            {
                double value = 1;
                for(int j = 0 ; j < coefs.size() ; j++)
                {
                    if(i!=j) value *= x[i]-x[j];
                }
                coefs[i] = 1.0/value*y[i];
            }
        }
        double calcXCoef(double xVal, int i )
        {
            double value = 1;
            for(int j = 0 ; j < x.size() ; j++)
            {
                if(i!=j) value *= xVal-x[j];
            }
            return value;
        }
        double calcY(double xVal)
        {
            double value=0;
            for(int i = 0 ; i < coefs.size() ; i++)
            {
                value+=calcXCoef(xVal, i)*coefs[i];
            }
            return value;
        }
};

void saveCSV(ofstream &file, vector<double> x, vector<double> y)
{
    for(int i = 0 ; i < x.size() ; i++)
    {
        file << x[i] << ", " << y[i] << endl;
    }
}

vector<double> generateValues(vector<double> x, function<double(double)> yFunc)
{
    vector<double> y(x.size());
    for(int i = 0 ; i < y.size() ; i++)
    {
        y[i] = yFunc(x[i]);
    }
    return y;
}

pair<vector<double>, vector<double> > generateXY(function<double(double)> xFunc, function<double(double)> yFunc, vector<double> k)
{
    vector<double> x;
    vector<double> y;
    for (auto i1 = begin(k); i1 != end(k); ++i1)
    {
        x.push_back(xFunc(*i1));
        y.push_back(yFunc(*i1));
    }
    return make_pair(x,y);
}

int main()
{
    int n = 4;
    int N = n+1;
    int NSave = 20;
    vector<double> x, y;
    vector<double> k(N);

    std::iota(k.begin(),k.end(), 0.0);
    std::tie(x,y) = generateXY([n](double k)->double{return xDep(k,n);}, [n](double k)->double{return initFunc(xDep(k, n));}, k);

    PolynomialInterpolation interp(x,y);

    double x0 = x[0];
    double dx = (x[x.size()-1]-x[0])/(NSave-1);

    vector<double> xForSave(NSave);
    std::generate(xForSave.begin(), xForSave.end(), [&x0,dx]()->double{double val = x0; x0+=dx; return val;});
    vector<double> yForSave = generateValues(xForSave, [&interp](double x)->double{return interp.calcY(x);});

    vector<double> yError = generateValues(xForSave, [&interp](double x)->double{return fabs(interp.calcY(x)-initFunc(x));});

    ofstream file("data.csv");
    saveCSV(file, xForSave, yForSave);
    file.close();
    ofstream file2("dataExact.csv");
    saveCSV(file2, x, y);
    file2.close();
    ofstream file3("dataError.csv");
    saveCSV(file3, xForSave, yError);
    file3.close();
}
