set ylabel "A"
set xlabel "Hz"


plot "freq.csv" using 1:2 with boxes title "DFT sin(x) [0:14.5*Pi]", "freqHanna.csv" using 1:2 with boxes title "DFT sin(x) [0:14.5*Pi] Hann window"
