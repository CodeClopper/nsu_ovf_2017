#include <fftw3.h>
#include<cmath>
#include<functional>
#include<fstream>
#include<algorithm>
#include"vectorOperations.h"
using namespace std;

double f(double t, double omega)
{
    return sin(omega*t);
}

double kannaWindow(double t, double t1, double t2)
{
    return sin((t-t1)*M_PI/(t2-t1));
}

vector<double> generateFunctionValues(const vector<double>& t, std::function<double(double)> f)
{
    vector<double> res(t);
    for(double &val: res)
    {
        val = f(val);
    }
    return res;
}

vector<double> calcAbs(fftw_complex* yF, int N)
{
    vector<double> res(N);
    for(int i = 0 ; i < N ; i++)
    {
        res[i] = sqrt(yF[i][0]*yF[i][0] + yF[i][1]*yF[i][1]);
    }
    return res;
}

void csvFormat(ofstream &file, const vector<double>& x, const vector<double>& y)
{
    for(int i = 0 ; i < x.size() ; i++)
    {
        file << x[i] << " " << y[i] << endl;
    }
    file << endl;
}

int main()
{
    const int N = 256;

    double t0 = 0;
    double t1 = M_PI*14.5;

    double dt = (t1-t0)/(N-1);
    double omega = 1;

    vector<double> t(N);
    vector<double> freq(N);
    for(int i = 0 ; i < N ; i++)
    {
        t[i] = t0 + i*dt;
    }
    for(int i = 0 ; i < N ; i++)
    {
        freq[i] = double(i)/(t1-t0);
    }
    vector<double> y = generateFunctionValues(t, [=](double t){return f(t,omega);});
    vector<double> yWindow = generateFunctionValues(t, [=](double t){return f(t,omega)*kannaWindow(t, t0, t1);});

    fftw_complex *out;
    fftw_complex *outHanna;
    fftw_plan p;
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    outHanna = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    p = fftw_plan_dft_r2c_1d(N, yWindow.data(), outHanna, FFTW_ESTIMATE);
    fftw_execute(p);
    p = fftw_plan_dft_r2c_1d(N, y.data(), out, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);
    vector<double> yFAbs = calcAbs(out, N);
    vector<double> yHannaFAbs = calcAbs(outHanna, N);
    fftw_free(out);
    fftw_free(outHanna);
    cout << yFAbs << yHannaFAbs << endl;

    ofstream sig("sig.csv");
    csvFormat(sig, t, y);
    sig.close();

    ofstream freqFile("freq.csv");
    csvFormat(freqFile, freq, yFAbs);
    freqFile.close();

    ofstream sigHanna("sigHanna.csv");
    csvFormat(sigHanna, t, yWindow);
    sigHanna.close();

    ofstream freqFileHanna("freqHanna.csv");
    csvFormat(freqFileHanna, freq, yHannaFAbs);
    freqFileHanna.close();
}
