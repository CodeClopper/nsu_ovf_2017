#pragma once

#include<vector>
#include<cmath>
#include<iostream>

std::vector<double> operator+(const std::vector<double>& v1, const std::vector<double>& v2)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]+v2[i];
    }
    return std::move(res);
}

std::vector<double> operator+(const std::vector<double>& v1, std::vector<double>&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] += v1[i];
    }
    return std::move(v2);
}

std::vector<double> operator+(std::vector<double>&& v1, const std::vector<double>& v2)
{
    return std::move(v2+v1);
}

std::vector<double> operator+(std::vector<double>&& v1, std::vector<double>&& v2)
{
    return std::move(v1+v2);
}

std::vector<double> operator-(const std::vector<double>& v1, const std::vector<double>& v2)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]-v2[i];
    }
    return std::move(res);
}

std::vector<double> operator-(const std::vector<double>& v1, std::vector<double>&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] = v1[i]-v2[i];
    }
    return std::move(v2);
}

std::vector<double> operator-(std::vector<double>&& v1, const std::vector<double>& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] = v1[i]-v2[i];
    }
    return std::move(v1);
}

std::vector<double> operator-(std::vector<double>&& v1, std::vector<double>&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] = v1[i]-v2[i];
    }
    return std::move(v1);
}

std::vector<double> operator+(const std::vector<double>& v1, double a)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]+a;
    }
    return std::move(res);
}

std::vector<double> operator+(std::vector<double>&& v1, double a)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] += a;
    }
    return std::move(v1);
}

std::vector<double> operator+(double a, const std::vector<double>& v1)
{
    return std::move(v1+a);
}

std::vector<double> operator+(double a, std::vector<double>&& v1)
{
    return std::move(v1+a);
}

std::vector<double> operator/(const std::vector<double>& v1, double a)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]/a;
    }
    return std::move(res);
}

std::vector<double> operator*(const std::vector<double>& v1, double a)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*a;
    }
    return std::move(res);
}

std::vector<double> operator*(std::vector<double>&& v1, double a)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] *= a;
    }
    return std::move(v1);
}

std::vector<double> operator*(double a, const std::vector<double>& v1)
{
    return std::move(v1*a);
}

std::vector<double> operator*(double a, std::vector<double>&& v1)
{
    return std::move(v1*a);
}

std::vector<double> operator*(const std::vector<double>& v1, const std::vector<double>& v2)
{
    std::vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*v2[i];
    }
    return std::move(res);
}

std::vector<double> operator*(const std::vector<double>& v1, std::vector<double>&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] *= v1[i];
    }
    return std::move(v2);
}

std::vector<double> operator*(std::vector<double>&& v1, const std::vector<double>& v2)
{
    return std::move(v2*v1);
}

double norm(const std::vector<double>& v)
{
    double norm_val=0;
    for(auto val:v)
    {
        norm_val+=val*val;
    }
    return std::sqrt(norm_val);
}

double integral(const std::vector<double>& v, double dx)
{
    double norm_val=0;
    for(auto val:v)
    {
        norm_val+=dx*val*val;
    }
    return norm_val;
}

std::ostream& operator<<(std::ostream& out, const std::vector<double> &vec)
{
    for(auto v: vec)
    {
        out << v << std::endl;
    }
    return out;
}
