set ylabel "y"
set xlabel "t"


plot "sig.csv" using 1:2 with lines title "sin(x) [0:14.5*Pi]", "sigHanna.csv" using 1:2 with lines title "sin(x) [0:14.5*Pi] Hann window"
