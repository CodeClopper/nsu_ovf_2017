from math import tan, sqrt

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from abc import ABC, abstractmethod

class Function:
    def __init__(self):
        f = open("config.yaml", 'r')
        settings = load(f, Loader=Loader)
        f.close()
        self.a = float(settings['a'])
        self.U0 = float(settings['U0'])
    def val(self,x):
        return 1/tan(sqrt(2.0*self.a**2*self.U0*(1-x)))-sqrt(1/x-1)

energyLevel = Function()

print(energyLevel.val(0.6))

class Abstract_method(ABC):
    @abstractmethod
    def solve(self):
        pass

class Bisection(Abstract_method):
    def __init__(self, x0, x1, iterLimit, function_to_solve):
        self.x0 = x0
        self.x1 = x1
        self.iterLimit = iterLimit
        self.function_to_solve = function_to_solve
        
    def solve(self):
        for i in range(iterLimit):
            val = (self.x1-self.x0)/2.0
            
