#include<cstdio>
#include<bitset>
#include<string>
#include<iostream>
#include<fstream>
#include<cmath>
#include<iomanip>

using namespace std;

typedef double (* functionCall)(double arg);

template <class Type>
class visualisation
{
    public:
        const Type &function;
        double x0,x1;
        int N;

        visualisation(Type &function, double x0, double x1, int N)
        :
            function(function),
            x0(x0),
            x1(x1),
            N(N)
        {

        }

        void outputInFile(const string filename)
        {
            ofstream myfile;
            myfile.open("function.csv");
            double dx = (x1-x0)/(N-1);
            for(int i = 0 ; i < N ; i++)
            {
                double x = x0 + dx*i;
                myfile << x << ", " << function.function(x) << endl;
            }
            myfile.close();
        }
};



class energyLevel
{
public:
    double a;
    double U0;
    energyLevel(double a, double U0)
    :
    a(a),
    U0(U0)
    {

    }
    double function(double x) const
    {
        return 1.0/tan(sqrt(2.0*a*a*U0*(1.0-x)))-sqrt(1.0/x-1.0);
    }
};

double bisection(double x0, double x1, int iterMax, double threshold, energyLevel &level)
{
    double val;
    for(int i = 0 ; i < iterMax ; i++)
    {
        val = (x0+x1)/2.0;
        if(fabs(x1-x0)<threshold)
        {
            return val;
        }
        if(signbit(level.function(x0))==signbit(level.function(val)))
        {
            x0 = val;
        }
        else
        {
            x1 = val;
        }
    }
    cout << "error" << endl;
    return val;
}

int main()
{
    energyLevel level(1,1);
    cout << bisection(0.0001,0.9999,10000,0.0001,level) << endl;
    visualisation<energyLevel> vis(level, 0.001,0.999, 30);
    vis.outputInFile("data.csv");
}
