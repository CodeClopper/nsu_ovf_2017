#include <iostream>
#include <functional>
#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

double integral4(double x)
{
    return (1.0+exp(-x))/(1.0+pow(x,3.0/2.0));
}

double simpson(double a, double b, std::function<double(double)> &functionToSolve)
{
    double x0 = a;
    double x2 = b;
    double x1 = (x2+x0)*0.5;
    return (x2-x0)/6.0*(functionToSolve(x0)+4.0*functionToSolve(x1)+functionToSolve(x2));
}

double xInf(double t)
{
    return t/(1.0-t);
}

std::function<double(double)> infFunc(std::function<double(double)> f, std::function<double(double)> x)
{
    return [f,x](double t) -> double {
        return f(x(t))/pow(1.0-t,2.0);
    };
}

double xAsymp(double t, double a, double b)
{
    return 0.5*(a + b) + 0.5*(b - a)*tanh(t);
}

std::function<double(double)> asympFunc(double a, double b, std::function<double(double)> f, std::function<double(double, double, double)> x)
{
    return [a,b,f,x](double t) -> double {
        return f(x(t, a, b)) * (b - a) * 0.5 / pow(cosh(t), 2.0);
    };
}

double simpsonSolve(double a, double b, std::function<double(double)> functionToSolve, int N)
{
    double dx = (b-a)/(N-1);
    double integral = 0;
    for(int i = 0 ; i < N-1 ; i++)
    {
        double aLoc = a+dx*i;
        double bLoc = a+dx*(i+1);
        integral += simpson(aLoc, bLoc, functionToSolve);
    }
    return integral;
}

int main()
{

    cout << simpsonSolve(-16.0, 16.0,
                         asympFunc(0.0, 1.0,
                                   infFunc(integral4,
                                           xInf),
                                   xAsymp),
                         80) << endl;
}
