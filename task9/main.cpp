#include<iostream>
#include<functional>
#include<fstream>
#include<utility>
#include<algorithm>
#include<vector>
#include<memory>
#include<string>
#include<initializer_list>
#include"vectorOperations.h"

using namespace std;

int index(int i, int j, int N1)
{
    return i+j*N1;
}

class Matrix
{
public:
    std::vector<double> data;
    int N1;
    int N2;
    Matrix(initializer_list<double>vals, int N1, int N2)
    : data(vals), N1(N1), N2(N2)
    {
    }
    Matrix(int N1, int N2)
    : data(N1*N2, 0.0), N1(N1), N2(N2)
    {
    }
    Matrix()
    : data(), N1(0), N2(0)
    {
    }
    Matrix(const Matrix& a) : data(a.data), N1(a.N1), N2(a.N2) { }
    int index(int i, int j) const
    {
        return i*N2+j;
    }
    friend Matrix operator*(const Matrix& lhs, const Matrix& rhs)
    {
        Matrix res(lhs.N1, rhs.N2);
        for(int i = 0 ; i < lhs.N1 ; i++)
        {
            for(int j = 0 ; j < rhs.N2 ; j++)
            {
                for(int k = 0 ; k < lhs.N2 ; k++)
                {
                    res[res.index(i, j)] += lhs[lhs.index(i, k)] * rhs[rhs.index(k, j)];
                }
            }
        }
        return res;
    }
    friend vector<double> operator*(const Matrix& v1, const vector<double>& v2)
    {
        vector<double> res(v1.N1);
        for(int i = 0 ; i < v1.N1 ; i++)
        {
            for(int j = 0 ; j < v2.size() ; j++)
            {
                res[i] += v1[v1.index(i,j)]*v2[j];
            }
        }
        return res;
    }
    friend vector<double> operator*(const vector<double>& v1, const Matrix& v2)
    {
        vector<double> res(v2.N2);
        for(int i = 0 ; i < v2.N2 ; i++)
        {
            for(int j = 0 ; j < v1.size() ; j++)
            {
                res[i] += v1[j]*v2[v2.index(j,i)];
            }
        }
        return res;
    }
    double& operator[](std::size_t idx)       { return data[idx]; }
    const double& operator[](std::size_t idx) const { return data[idx]; }
    friend ostream& operator<<(ostream& os, const Matrix& m)
    {
        for(int i = 0 ; i < m.N1 ; i++)
        {
            for(int j = 0 ; j < m.N2 ; j++)
            {
                os << m.data[m.index(i,j)] << " ";
            }
            os << endl;
        }
        return os;
    }
    Matrix& operator=(const Matrix& source)
    {
        data = source.data;
        N1 = source.N1;
        N2 = source.N2;
        return *this;
    }
};

double function1(const vector<double> &var)
{
    return 998.0*var[0] + 1998.0*var[1];
}

double function2(const vector<double> &var)
{
    return -999.0*var[0] - 1999.0*var[1];
}

std::function<vector<double>(const vector<double> &, const vector<double> &)> functionEulerGen(std::function<vector<double>(const vector<double>&)> f, double h)
{
    return [=](const vector<double> &vars, const vector<double> &v0) -> vector<double> {
        return vars - v0 - h*f(vars);
    };
}

Matrix generateJacobiMatrix(std::function<vector<double>(const vector<double> &)> f, const vector<double> &v, const vector<double> &h)
{
    Matrix jac(v.size(),v.size());
    for(int i = 0 ; i < v.size() ; i++)
    {
        std::function<double(const vector<double> &)> fLoc = [f,i](const vector<double> & val){
            return f(val)[i];
        };
        for(int j = 0 ; j < v.size() ; j++)
        {
            vector<double> v0 = v;
            vector<double> v1 = v;
            v0[j] -= h[j]*0.5;
            v1[j] += h[j]*0.5;
            jac[jac.index(i,j)] = (fLoc(v1)-fLoc(v0))/h[j];
        }
    }
    return jac;
}

vector<double> NewtonIteration(std::function<vector<double>(vector<double>)> f, Matrix revJac, const vector<double> &var)
{
    return var - (revJac*f(var));
}

Matrix inverse2x2Matrix(const Matrix& m)
{
    Matrix res(m);
    double det = 1.0/(res[0]*res[3]-res[1]*res[2]);
    res.data[0] = m.data[3]*det;
    res.data[1] = -m.data[1]*det;
    res.data[2] = -m.data[2]*det;
    res.data[3] = m.data[0]*det;
    return res;
}

ostream& operator<<(ostream& os, const vector<double>& m)
{
    for(int i = 0 ; i < m.size() ; i++)
    {
        os << m[i] << " ";
    }
    return os;
}

void csvFormat(ofstream &file, vector<vector<double> > variables)
{
    for(int i = 0 ; i < variables.size() ; i++)
    {
        file << variables[i] << endl;
    }
    file << endl;
}

vector<vector<double> > solveSystem(std::function<vector<double>(const vector<double>&, const vector<double>&)> functions,
                                    vector<double> v0,
                                    const int solveIter,
                                    const int newtonIter,
                                    const vector<double> h)
{
    vector<vector<double> > res;

    for(int i = 0 ; i < solveIter ; i++)
    {
        vector<double> v1 = v0;
        auto fToSolve = std::bind(functions, std::placeholders::_1, v0);
        Matrix jacM = generateJacobiMatrix(fToSolve, v0, h);
        Matrix invJacM = inverse2x2Matrix(jacM);
        for(int j = 0 ; j < newtonIter ; j++)
        {
            v1 = NewtonIteration(fToSolve, invJacM, v1);
        }
        //cout << v1 << endl;
        v0 = v1;
        res.push_back(v1);
    }
    return res;
}

void solveSeries(std::function<vector<double>(const vector<double>&, const vector<double>&)> functions,
                 vector<vector<double> > v0List,
                 const int solveIter,
                 const int newtonIter,
                 const vector<double> h)
{
    for(int i = 0 ; i < v0List.size() ; i++)
    {
        vector<vector<double> > res = solveSystem(functions, v0List[i], solveIter, newtonIter, h);
        ofstream file("data"+std::to_string(i)+".csv");
        csvFormat(file, res);
        file.close();
    }
}

int main()
{
    std::function<vector<double>(const vector<double>&, const vector<double>&)> functions = functionEulerGen([](const vector<double>& var) -> vector<double> {
        vector<double> res(2);
        res[0] = function1(var);
        res[1] = function2(var);
        return res;
    }, 0.00001);

    /*vector<double> v0 = {0.01 , 0.01};

    vector<vector<double> > res = solveSystem(functions, v0, 10000, 400, {0.001, 0.001});
    ofstream file("data.csv");
    csvFormat(file, res);
    file.close();*/
    vector<vector<double> > v0List = {{-0.01 , 0.01},{0.0 , 0.01},{0.01 , 0.01},
                                      {-0.01 , 0.0},{0.0 , 0.0},{0.01 , 0.0},
                                      {-0.01 , -0.01},{0.0 , -0.01},{0.01 , -0.01}};
    solveSeries(functions, v0List, 100, 400, {0.001,0.001});
}
