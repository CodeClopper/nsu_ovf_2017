
using namespace std;

vector<double> operator*(const vector<double>& v1, const vector<double>& v2)
{
    vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*v2[i];
    }
    return res;
}

vector<double> operator*(const vector<double>& v1, double v2)
{
    vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*v2;
    }
    return res;
}

vector<double> operator*(double v1, const vector<double>& v2)
{
    vector<double> res(v2.size());
    for(int i = 0 ; i < v2.size() ; i++)
    {
        res[i] = v1*v2[i];
    }
    return res;
}

vector<double> operator+(const vector<double>& v1, const vector<double>& v2)
{
    vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]+v2[i];
    }
    return res;
}

vector<double> operator-(const vector<double>& v1, const vector<double>& v2)
{
    vector<double> res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]-v2[i];
    }
    return res;
}
