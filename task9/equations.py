from sympy import *

u = IndexedBase('u')
v = IndexedBase('v')

i = symbols('i', cls=Idx)
h = symbols('h')

#f = Function('f')

def f(x,y):
    return 998*x + 1998*y;

def g(x,y):
    return -999*x - 1999*y;

eq = [Eq( u[i+1] , u[i] + h*f(u[i+1],v[i+1]) ), Eq( v[i+1] , v[i] + h*g(u[i+1],v[i+1]) )]

M = Matrix([ [ 998, 1998] , [ -999, -1999] ])

fM = Matrix([[u[i]],[v[i]]])

print(M*fM*h+fM)

print(M.inv())

print(eq)
print(solve(eq,[u[i+1],v[i+1]]))
