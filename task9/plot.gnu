set ylabel "v"
set xlabel "u"

plot "data0.csv" using 1:2 with lines title "uv0 = (-0.01, 0.01)", \
     "data1.csv" using 1:2 with lines title "uv0 = (0, 0.01)", \
     "data2.csv" using 1:2 with lines title "uv0 = (0.01, 0.01)", \
     "data3.csv" using 1:2 with lines title "uv0 = (-0.01, 0)", \
     "data4.csv" using 1:2 with lines title "uv0 = (0, 0)", \
     "data5.csv" using 1:2 with lines title "uv0 = (0.01, 0)", \
     "data6.csv" using 1:2 with lines title "uv0 = (-0.01, -0.01)", \
     "data7.csv" using 1:2 with lines title "uv0 = (0, -0.01)", \
     "data8.csv" using 1:2 with lines title "uv0 = (0.01, -0.01)"
