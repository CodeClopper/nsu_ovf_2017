#include<iostream>
#include<functional>
#include<fstream>
#include<utility>
#include<algorithm>
#include<vector>
#include<memory>
#include <string>

using namespace std;

vector<double> operator+(const vector<double>& v1, const vector<double>& v2)
{
    vector<double> res(v1);
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i]+=v2[i];
    }
    return res;
}

vector<double> operator*(const vector<double>& v1, const double& v2)
{
    vector<double> res(v1);
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i]*=v2;
    }
    return res;
}

vector<double> operator*(const double& v2, const vector<double>& v1)
{
    vector<double> res(v1);
    res = v1*v2;
    return res;
}

vector<double> processFunctions(vector<double> variables, vector<std::function<double(vector<double>)> > functions)
{
    vector<double> newValues(variables);
    for(int i = 0 ; i < variables.size() ; i++)
    {
        newValues[i] = functions[i](variables);
    }
    return newValues;
}

vector<double> EulerStep(vector<double> variables, double dt, vector<std::function<double(vector<double>)> > functions)
{
    vector<double> newValues(variables);
    for(int i = 0 ; i < variables.size() ; i++)
    {
        newValues[i] = variables[i] + dt*functions[i](variables);
    }
    return newValues;
}

vector<double> RungeStep(vector<double> variables, double dt, vector<std::function<double(vector<double>)> > functions)
{
    vector<double> newValues(variables);
    for(int i = 0 ; i < variables.size() ; i++)
    {
        newValues[i] = variables[i] + dt*functions[i](variables + 0.5*dt*processFunctions(variables, functions));
    }
    return newValues;
}

void csvFormatTime(ofstream &file, double t, vector<double> variables)
{
    file << t;
    for(auto var: variables)
    {
        file << ", " << var ;
    }
    file << endl;
}

void csvFormat(ofstream &file, vector<double> variables)
{
    file << variables[0];
    for(int i = 1 ; i < variables.size() ; i++)
    {
        file << ", " << variables[i] ;
    }
    file << endl;
}

void calculateSeries(string fileName,
                     vector<vector<double> > initVariablesPack,
                     vector<std::function<double(vector<double>)> > functions,
                     std::function<vector<double>(vector<double>, double, vector<std::function<double(vector<double>)> >)> method)
{
    for(int i = 0 ; i < initVariablesPack.size() ; i++)
    {
        vector<double> variables(initVariablesPack[i]);

        ofstream file(fileName + to_string(i) + string(".csv"));
        double t = 0;
        double dt = 0.001;
        for(int i = 0 ; i < 100 ; i++)
        {
            variables = method(variables, dt, functions);
            t+=dt;
            csvFormatTime(file, t, variables);
        }
        file.close();
    }
}

void calculateSeries2(string name,
                     vector<vector<double> > initVariablesPack,
                     vector<std::function<double(vector<double>)> > functions,
                     std::function<void(vector<double>, vector<std::function<double(vector<double>)> >, string)> method)
{
    for(int i = 0 ; i < initVariablesPack.size() ; i++)
    {
        string fileName(name + to_string(i) + string(".csv"));
        method(initVariablesPack[i], functions, fileName);
    }
}

std::function<void(vector<double>, vector<std::function<double(vector<double>)> >, string)>
methodBuilder(double t0,
              double dt,
              int N,
              std::function<void(ofstream&, double, vector<double>)> format,
              std::function<vector<double>(vector<double>, double, vector<std::function<double(vector<double>)> >)> method)
{
    shared_ptr<double> t = make_shared<double>(t0);
    return [t, t0,dt,N,&method,&format](vector<double> initVariablesPack, vector<std::function<double(vector<double>)> > functions, string name) mutable
    {
        vector<double> variables(initVariablesPack);
        *t = t0;
        ofstream file(name);
        for(int i = 0 ; i < N ; i++)
        {
            format(file, *t, variables);
            variables = method(variables, dt, functions);
            *t+=dt;

        }
        file.close();
    };
}

/*void saveData(string name, vector<std::pair<double,double> > values, std::function<void(ofstream &, double, vector<double>)> format)
{
    ofstream file(name);

    for (auto value: values)
    {
        csvFormat(file, value);
    }

    file.close();
}*/

int main()
{
    vector<std::function<double(vector<double>)> > functions;

    functions.push_back([]() mutable -> std::function<double(vector<double>)>{
        double a = 10;
        double b = 2;
        return [=](vector<double> variables) mutable -> double {
            double x = variables[0];
            double y = variables[1];
            return a*x - b*x*y;
        };
    }()
    );
    functions.push_back([]() mutable -> std::function<double(vector<double>)>{
        double c = 2;
        double d = 10;
        return [=](vector<double> variables) mutable -> double {
            double x = variables[0];
            double y = variables[1];
            return c*x*y - d*y;
        };
    }()
    );

    vector<double> variables(2);
    variables[0] = 20;
    variables[1] = 30;

    std::function<void(vector<double>, vector<std::function<double(vector<double>)> >, string)> methodEuler = methodBuilder(0,0.001,1000,csvFormatTime,RungeStep);
    vector<vector<double> > initVariablesPack = {{10,10}, {10,20}, {10,30},
                                                 {20,10}, {20,20}, {20,30},
                                                 {30,10}, {30,20}, {30,30}};
    calculateSeries2("plot2/solutionEuler", initVariablesPack, functions, methodEuler);

    /*ofstream file("plot/solution3.csv");
    double t = 0;
    double dt = 0.001;
    for(int i = 0 ; i < 100 ; i++)
    {
        variables = EulerStep(variables, dt, functions);
        t+=dt;
        csvFormatTime(file, t, variables);
    }
    file.close();*/

    /*variables[0] = 20;
    variables[1] = 30;

    ofstream file3("plot/solutionRunge3.csv");
    t = 0;
    dt = 0.001;
    for(int i = 0 ; i < 100 ; i++)
    {
        variables = RungeStep(variables, dt, functions);
        t+=dt;
        csvFormatTime(file3, t, variables);
    }
    file3.close();*/
}
