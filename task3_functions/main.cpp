#include<utility>
#include<functional>
#include<cmath>
#include<tuple>
#include<iostream>

using namespace std;

pair<pair<double, double>, 
pair<double, double> > 
returnSegPair(pair<double, double> segment)
{
    double x1 = segment.first;
    double x2 = segment.second;
    double c = (x2+x1)*0.5;
    pair<double, double> first_segment(x1, c);
    pair<double, double> second_segment(c, x2);
    return make_pair(first_segment, second_segment);
}

pair<double, double>
rootSegment(pair<double, double> segment1, pair<double, double> segment2, function<double(double)> f)
{
    double x1 = segment1.first;
    double x2 = segment1.second;
    if(f(x1)*f(x2) < 0)
    {
        return segment1;
    }
    else
    {
        return segment2;
    }
}

double mag(pair<double, double> segment)
{
    double x1 = segment.first;
    double x2 = segment.second;
    return sqrt(pow(x1-x2,2.0));
}

double f(double x)
{
    double a = 1;
    double U0 = 1;
    return 1.0/tan(sqrt(2.0*a*a*U0*(1.0-x)))-sqrt(1.0/x-1.0);
}

double bisection(function<double(double)> f, pair<double, double> initSegment, double eps, int N)
{
    pair<double, double> segment = initSegment;
    for(int i = 0 ; i < N ; i++)
    {
        pair<double, double> leftSegment;
        pair<double, double> rightSegment;
        tie(leftSegment, rightSegment) = returnSegPair(segment);
        
        segment = rootSegment(leftSegment, rightSegment, f);
        cout << "[" << segment.first << "," << segment.second << "] " << mag(segment) << endl;
        if(mag(segment) < eps)
        {
            return (segment.first+segment.second)*0.5;
        }
    }
    return -1;
}

int main()
{
    pair<double, double> initSegment(0.0001,0.9999);
    double root = bisection(f, initSegment, 0.00001, 52);
    cout << "bisection method root xi = " \
         << root \
         << endl;
}

