#include <iostream>
#include <functional>
#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

double integral1(double x)
{
    return 1.0/(1.0+pow(x,2.0));
}

double integral2(double x)
{
    return pow(x,1.0/3.0)*exp(sin(x));
}

double integral3(double x)
{
    return (1.0+cos(x))/(pow(x,1.0/3.0));
}

double integral4(double x)
{
    return (1.0+exp(-x))/(1.0+pow(x,3.0/2.0));
}

double integralNotConv(double x)
{
    return 1.0/x;
}

double simpson(const std::pair<double,double> &segment, std::function<double(double)> &functionToSolve)
{
    double x0 = segment.first;
    double x2 = segment.second;
    double x1 = (x2+x0)*0.5;
    return (x2-x0)/6.0*(functionToSolve(x0)+4.0*functionToSolve(x1)+functionToSolve(x2));
}

double trapez(const std::pair<double,double> &segment, std::function<double(double)> &functionToSolve)
{
    double x0 = segment.first;
    double x1 = segment.second;
    return (functionToSolve(x0)+functionToSolve(x1))*0.5*(x1-x0);
}

class StepSizeControl
{
    public:
        double a;
        double b;
        StepSizeControl(double a, double b)
        : a(a), b(b)
        {
        }
        StepSizeControl(const StepSizeControl &control)
        : a(control.a), b(control.b)
        {
        }
        virtual std::pair<double,double> step() = 0;
        virtual bool continueLoop() = 0;
        virtual void reset() = 0;
        virtual string info() = 0;
        /*virtual StepSizeControl& operator=(const StepSizeControl &control1)
        {

        }*/
};

class StepSizeControlLinear : public StepSizeControl
{
    public:
        int N;
        int i;
        StepSizeControlLinear(double a, double b, int N)
        : StepSizeControl(a,b), N(N)
        {
            i = 0;
        }
        StepSizeControlLinear(const StepSizeControlLinear &control)
        : StepSizeControl(control),
          N(control.N),
          i(control.i)
        {
        }
        virtual std::pair<double,double> step()
        {
            double dx = (b-a)/(N-1);
            std::pair<double,double> segment = {a+dx*i,a+dx*(i+1)};
            i++;
            return segment;
        }
        virtual bool continueLoop()
        {
            if(i<N-1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        virtual void reset()
        {
            i = 0;
        }
        virtual string info()
        {
            return "StepSizeControlLinear N = " + std::to_string(N) + " interval = [" + std::to_string(a) + "," + std::to_string(b) + "]";
        }
        /*StepSizeControlLinear& operator=(StepSizeControl control1)
        {
            a = control1.a;
            b = control1.b;
            N = control1.N;
            i = 0;
            return *this;
        }
        StepSizeControlLinear& operator=(const StepSizeControl &control1)
        {
            a = control1.a;
            b = control1.b;
            N = control1.N;
            i = 0;
            return *this;
        }*/
};

class StepSizeControlIncreasing : public StepSizeControl
{
    public:
        double pos;
        double dx0;
        double dx;
        double dxMultip;
        int i;
        StepSizeControlIncreasing(double a, double b, double dx0, double dxMultip)
        : StepSizeControl(a,b), pos(a), dx0(dx0), dx(dx0), dxMultip(dxMultip)
        {
            i = 0;
        }
        StepSizeControlIncreasing(const StepSizeControlIncreasing &control)
        : StepSizeControl(control),
          pos(control.pos),
          dx(control.dx),
          dxMultip(control.dxMultip),
          i(control.i)
        {
        }
        virtual std::pair<double,double> step()
        {
            double v1 = pos;
            double v2 = min(b,pos+dx);
            std::pair<double,double> segment = {v1,v2};
            pos+=dx;
            dx*=dxMultip;
            i++;
            return segment;
        }
        virtual bool continueLoop()
        {
            if(pos>=a && pos<b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        virtual void reset()
        {
            pos = a;
            i = 0;
            dx = dx0;
        }
        virtual string info()
        {
            return "StepSizeControlIncreasing interval = [" + std::to_string(a) + "," + std::to_string(b) + "] dxMultip = " + std::to_string(dxMultip)+ " dx0 = " + std::to_string(dx0);
        }
};

class StepSizeControlFunction : public StepSizeControl
{
    public:
        double pos;
        double multip;
        std::function<double(double)> stepFunction;
        double maxStep;
        int i;
        StepSizeControlFunction(double a, double b, double multip, double maxStep, std::function<double(double)> stepFunction)
        : StepSizeControl(a,b), pos(a), multip(multip), maxStep(maxStep), stepFunction(stepFunction)
        {
            i = 0;
        }
        StepSizeControlFunction(const StepSizeControlFunction &control)
        : StepSizeControl(control),
          pos(control.pos),
          multip(control.multip),
          stepFunction(control.stepFunction),
          i(control.i)
        {
        }
        virtual std::pair<double,double> step()
        {
            double dx = multip*stepFunction(pos);
            //cout <<"dx1=" << dx << endl;
            //dx = multip*stepFunction(pos+dx*0.5);
            //cout <<"dx2=" << dx << endl;
            //dx = multip*stepFunction(pos+dx*0.5);
            //cout <<"dx3=" << dx << endl;
            //dx = min(multip*stepFunction(pos+dx*0.5),maxStep);
            //cout <<"dx4=" << dx << endl;
            double v1 = pos;
            double v2 = min(b,pos+dx);
            std::pair<double,double> segment = {v1,v2};
            pos+=dx;
            i++;
            return segment;
        }
        virtual bool continueLoop()
        {
            if(pos>=a && pos<b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        virtual void reset()
        {
            pos = a;
            i = 0;
        }
        virtual string info()
        {
            return "StepSizeControlIncreasing interval = [" + std::to_string(a) + "," + std::to_string(b) + "] multip = " + std::to_string(multip);
        }
};

class IntegralCalculator
{
    public:
        StepSizeControl *control;
        std::function<double(double)> functionToSolve;
        std::function<double(const std::pair<double,double>&, std::function<double(double)>&)> integralOnInterval;
        string name;

        IntegralCalculator(StepSizeControl *control, std::function<double(double)> functionToSolve, std::function<double(const std::pair<double,double>&, std::function<double(double)>&)> integralOnInterval, string name)
        : control(control), functionToSolve(functionToSolve), integralOnInterval(integralOnInterval), name(name)
        {
        }
        ~IntegralCalculator()
        {
            delete control;
        }
        void setStepSizeControl(StepSizeControl *control1)
        {
            delete control;
            control = control1;
        }
        /*StepSizeControl &getStepSizeControl()
        {
            return control;
        }*/
        double solve()
        {
            double integral = 0;
            while(control->continueLoop())
            {
                std::pair<double,double> segment = control->step();
                integral += integralOnInterval(segment,functionToSolve);
            }
            control->reset();
            return integral;
        }
        void printRes()
        {
            cout << control->info() << " Res = " << solve() << " " << name << endl;
        }
};

class IntegralCalculatorVector
{
    public:
        StepSizeControl *control;
        std::function<double(double)> functionToSolve;
        std::function<double(const std::pair<double,double>&, std::function<double(double)>&)> integralOnInterval;
        std::vector<double> values;
        string name;

        IntegralCalculatorVector(StepSizeControl *control, std::function<double(double)> functionToSolve, std::function<double(const std::pair<double,double>&, std::function<double(double)>&)> integralOnInterval, string name)
        : control(control), functionToSolve(functionToSolve), integralOnInterval(integralOnInterval), name(name)
        {
        }
        ~IntegralCalculatorVector()
        {
            delete control;
        }
        void setStepSizeControl(StepSizeControl *control1)
        {
            delete control;
            control = control1;
        }
        /*StepSizeControl &getStepSizeControl()
        {
            return control;
        }*/
        double solve()
        {
            double integral = 0;
            while(control->continueLoop())
            {
                std::pair<double,double> segment = control->step();
                /*double x0 = segment.first;
                double x2 = segment.second;
                double x1 = (x2+x0)*0.5;
                integral += (x2-x0)/6.0*(functionToSolve(x0)+4.0*functionToSolve(x1)+functionToSolve(x2));*/
                values.push_back(integralOnInterval(segment,functionToSolve));
            }
            std::sort(values.begin(),values.end());
            control->reset();
            integral = std::accumulate(values.begin(),values.end(),0.0);
            values.clear();
            return integral;
        }
        void printRes()
        {
            cout << control->info() << " Res = " << solve() << " " << name << endl;
        }
};

int main()
{
    IntegralCalculator integral1_solve_simpson(new StepSizeControlLinear(-1,1,5),integral1, simpson,"1.0/(1.0+pow(x,2.0))");

    cout << "integral1 simpson" << endl;
    cout << std::setprecision(15);

    integral1_solve_simpson.printRes();
    integral1_solve_simpson.setStepSizeControl(new StepSizeControlLinear(-1,1,10));
    integral1_solve_simpson.printRes();
    integral1_solve_simpson.setStepSizeControl(new StepSizeControlLinear(-1,1,100));
    integral1_solve_simpson.printRes();
    integral1_solve_simpson.setStepSizeControl(new StepSizeControlLinear(-1,1,1000));
    integral1_solve_simpson.printRes();

    IntegralCalculator integral1_solve_trapez(new StepSizeControlLinear(-1,1,5),integral1, trapez,"1.0/(1.0+pow(x,2.0))");

    cout << "integral1 trapezoid" << endl;
    cout << std::setprecision(15);

    integral1_solve_trapez.printRes();
    integral1_solve_trapez.setStepSizeControl(new StepSizeControlLinear(-1,1,10));
    integral1_solve_trapez.printRes();
    integral1_solve_trapez.setStepSizeControl(new StepSizeControlLinear(-1,1,100));
    integral1_solve_trapez.printRes();
    integral1_solve_trapez.setStepSizeControl(new StepSizeControlLinear(-1,1,1000));
    integral1_solve_trapez.printRes();

    IntegralCalculator integral2_solve_simpson(new StepSizeControlLinear(0,1,5),integral2, simpson,"pow(x,1.0/3.0)*exp(sin(x))");
    integral2_solve_simpson.printRes();
    integral2_solve_simpson.setStepSizeControl(new StepSizeControlLinear(0,1,10));
    integral2_solve_simpson.printRes();
    integral2_solve_simpson.setStepSizeControl(new StepSizeControlLinear(0,1,100));
    integral2_solve_simpson.printRes();
    integral2_solve_simpson.setStepSizeControl(new StepSizeControlLinear(0,1,1000));
    integral2_solve_simpson.printRes();
    integral2_solve_simpson.setStepSizeControl(new StepSizeControlLinear(0,1,10000));
    integral2_solve_simpson.printRes();
    integral2_solve_simpson.setStepSizeControl(new StepSizeControlLinear(0,1,100000));
    integral2_solve_simpson.printRes();

    IntegralCalculator integral3_solve_simpson(new StepSizeControlIncreasing(0.1,1,0.01,1.1),integral3, simpson,"(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.1,1,0.001,1.1));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.001,1,0.00001,1.05));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.00001,1,0.00001,1.05));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.000000001,1,0.0000001,1.005));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.000000001,1,0.00000001,1.005));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.000000000001,1,0.00000000001,1.005));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.00000000000001,1,0.0000000000001,1.005));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.000000000000001,1,0.00000000000001,1.005));
    integral3_solve_simpson.printRes();
    integral3_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0000000000000001,1,0.000000000000001,1.005));
    integral3_solve_simpson.printRes();

    /*IntegralCalculatorVector integral3_solve_simpson_vector(new StepSizeControlIncreasing(0.1,1,0.01,1.1),integral3, simpson,"(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.1,1,0.001,1.1));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.01,1,0.00001,1.05));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.001,1,0.00001,1.05));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.00001,1,0.00001,1.05));
    integral3_solve_simpson_vector.printRes();

    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.000000001,1,0.00000001,1.005));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.00000000001,1,0.00000000001,1.001));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.0000000000001,1,0.0000000000001,1.0005));
    integral3_solve_simpson_vector.printRes();
    integral3_solve_simpson_vector.setStepSizeControl(new StepSizeControlIncreasing(0.000000000000001,1,0.000000000000001,1.00002));
    integral3_solve_simpson_vector.printRes();*/
    
    std::function<double(double)> variableSubst3 = [](double t)->double{double a = 0;
                                                                        double b = 1;
                                                                        double x = 0.5*(a+b)+0.5*(b-a)*tanh(t);
                                                                        return integral3(x)*(b-a)*0.5/pow(cosh(t),2.0);};
    
    IntegralCalculator integral3_solve_simpson_linear(new StepSizeControlLinear(-16.9,16.9,1000000),variableSubst3, simpson,"(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson_linear.printRes();
    
    IntegralCalculatorVector integral4_solve_simpson(new StepSizeControlIncreasing(0,100000000000,0.01,1.1),integral4, simpson,"(1.0+exp(-x))/(1.0+pow(x,3.0/2.0))");
    integral4_solve_simpson.printRes();
    
    std::function<double(double)> variableSubst4 = [](double t)->double{double x = t/(1.0-t);
                                                                        return integral4(x)/pow(1.0-t,2.0);};
    
    IntegralCalculatorVector integral4_solve_simpson_infty(new StepSizeControlLinear(0,0.999999999,100000),variableSubst4, simpson,"(1.0+exp(-x))/(1.0+pow(x,3.0/2.0))");
    integral4_solve_simpson_infty.printRes();
    
    std::function<double(double)> variableSubstSubst4 = [variableSubst4](double t)->double{double a = 0;
                                                                            double b = 1;
                                                                            double x = 0.5*(a+b)+0.5*(b-a)*tanh(t);
                                                                            return variableSubst4(x)*(b-a)*0.5/pow(cosh(t),2.0);};
    
    IntegralCalculatorVector integral4_solve_simpson_infty_asymp(new StepSizeControlLinear(-16.9,16.9,1000000),variableSubstSubst4, simpson,"(1.0+exp(-x))/(1.0+pow(x,3.0/2.0))");
    integral4_solve_simpson_infty_asymp.printRes();
    /*integral4_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.1,1,0.001,1.1));
    integral4_solve_simpson.printRes();
    integral4_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.01,1,0.00001,1.05));
    integral4_solve_simpson.printRes();
    integral4_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.001,1,0.00001,1.05));
    integral4_solve_simpson.printRes();
    integral4_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.00001,1,0.00001,1.05));
    integral4_solve_simpson.printRes();*/

    /*IntegralCalculator integral3_solve_simpson_linear(new StepSizeControlLinear(0.001,1,10000),integral3, simpson,"(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.000001,1,10000));
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.000000001,1,10000));
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.00000000001,1,10000));
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.001,1,1000000));
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.000000000000001,1,1000000));
    integral3_solve_simpson_linear.printRes();
    integral3_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.000000000000001,1,10000000));
    integral3_solve_simpson_linear.printRes();*/

    /*IntegralCalculator integral3_solve_simpson_function(new StepSizeControlFunction(1e-20, 1, 0.00000005, 1, [](double x) -> double {return 1.0/integral3(x);}),
                                                        integral3,
                                                        simpson,
                                                        "(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson_function.printRes();*/

    /*IntegralCalculator integral3_solve_simpson_function2(new StepSizeControlFunction(1e-20,
                                                                                    1,
                                                                                    1e10,
                                                                                    [](double x) -> double {return 1.0/fabs(-cos(x)/pow(x,1.0/3.0) + 2*sin(x)/(3*pow(x,4.0/3.0)) + 4*(cos(x) + 1.0)/(9*pow(x,7.0/3.0)));}),
                                                                                    integral3,
                                                                                    simpson,
                                                                                    "(1.0+cos(x))/(pow(x,1.0/3.0))");
    integral3_solve_simpson_function2.printRes();*/

    /*IntegralCalculator integral_not_conv_solve_simpson_linear(new StepSizeControlLinear(0.1,1,10),integralNotConv, simpson,"1.0/x");
    integral_not_conv_solve_simpson_linear.printRes();
    integral_not_conv_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.01,1,100));
    integral_not_conv_solve_simpson_linear.printRes();
    integral_not_conv_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.001,1,1000));
    integral_not_conv_solve_simpson_linear.printRes();
    integral_not_conv_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.0001,1,10000));
    integral_not_conv_solve_simpson_linear.printRes();
    integral_not_conv_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.00001,1,100000));
    integral_not_conv_solve_simpson_linear.printRes();
    integral_not_conv_solve_simpson_linear.setStepSizeControl(new StepSizeControlLinear(0.000001,1,1000000));
    integral_not_conv_solve_simpson_linear.printRes();

    IntegralCalculator integral_not_conv_solve_simpson(new StepSizeControlIncreasing(0.1,1,0.01,1.01),integralNotConv, simpson,"1.0/x");
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.01,1,0.001,1.01));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.001,1,0.0001,1.01));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0001,1,0.00001,1.01));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.00001,1,0.000001,1.01));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0000001,1,0.00000001,1.005));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.000000001,1,0.0000000001,1.001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.00000000001,1,0.000000000001,1.001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0000000000001,1,0.00000000000001,1.001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0000000000001,1,0.00000000000001,1.001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(0.0000000000001,1,0.00000000000001,1.0001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(1e-20,1,1e-20,1.0001));
    integral_not_conv_solve_simpson.printRes();
    integral_not_conv_solve_simpson.setStepSizeControl(new StepSizeControlIncreasing(1e-30,1,1e-30,1.0001));
    integral_not_conv_solve_simpson.printRes();*/
}
